Ext.define("Memo.view.List", {
	extend: "Ext.dataview.List",
	xtype: "memolist",
	config:{
		xtype:"list",
		data:[
			{title: "ham"},
			{title: "egg"},
			{title: "spam"}
		],
		itemTpl: "{title}"
	}
});
