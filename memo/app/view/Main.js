Ext.define('Memo.view.Main', {
	extend: "Ext.navigation.View",
    xtype: 'main',
    requires: [
    ],
    config: {
        items: [
			{
				title: "メモ",
				xtype: "memolist"
			}
        ]
    }
});
